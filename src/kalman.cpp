#include <tf/tf.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Twist.h"
#include "visualanalysis_msgs/BodyJoint2DArray.h"
#include "visualanalysis_msgs/BodyJoint3DArray.h"
#include "visualanalysis_msgs/BodyJoint3D.h"
#include "visualanalysis_msgs/BodyJoint2D.h"
#include "sensor_msgs/Imu.h"
#include <sstream>
#include <fstream>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core.hpp>
using namespace std;
#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using namespace cv;
namespace enc = sensor_msgs::image_encodings;

#include <fstream>

#include "ekf.h"

mat rotationMatrixFromEuler(vec eul)
{
  float roll = eul(0);
  float pitch = eul(1);
  float yaw = eul(2);

  mat Rz = {{cos(yaw), -sin(yaw), 0},
            {sin(yaw), cos(yaw), 0},
            {0, 0, 1}};

  mat Ry = {{cos(pitch), 0, sin(pitch)},
            {0, 1, 0},
            {-sin(pitch), 0, cos(pitch)}};

  mat Rx = {{1, 0, 0},
            {0, cos(roll), -sin(roll)},
            {0, sin(roll), cos(roll)}};

  mat R = (Rz * Ry) * Rx;
  return R;
}

mat translationMatrix(mat rotationMatrix, vec translationVector)

{
  mat translationMatrix = {{rotationMatrix(0, 0), rotationMatrix(0, 1), rotationMatrix(0, 2), translationVector(0)},
                           {rotationMatrix(1, 0), rotationMatrix(1, 1), rotationMatrix(1, 2), translationVector(1)},
                           {rotationMatrix(2, 0), rotationMatrix(2, 1), rotationMatrix(2, 2), translationVector(2)},
                           {0, 0, 0, 1}};
  return translationMatrix;
}

vec measureModel(vec state, vec inputs)
{
  colvec s1(24), s2(24), angles(24);
  s1 = state.subvec(10, 33);
  s2 = inputs;
  angles = s1 + s2;
  float lu = state(4);
  float lf = state(5);
  float l9 = state(6);
  float l3 = state(7);
  float l2 = state(8);
  float l1 = state(9);
  float xoffset = state(0);
  float yoffset = state(1);
  float zoffset = state(2);
  float fp = state(3);

  vec eul1 = angles.subvec(0, 2);
  vec eul2 = angles.subvec(3, 5);
  vec eul3 = angles.subvec(6, 8);
  vec eul4 = angles.subvec(9, 11);
  vec eul5 = angles.subvec(12, 14);
  vec eul6 = angles.subvec(15, 17);
  vec eul7 = angles.subvec(18, 20);
  vec eul8 = angles.subvec(21, 23);

  /*  mat Rotation = {{-1, 0, 0}, {0, -1, 0}, {0, 0, 1}};
    mat rot = {{cos(1), -sin(1), 0},
               {sin(1), cos(1), 0},
               {0, 0, 1}};*/
  mat rot = {{cos(1), -sin(1), 0},
             {sin(1), cos(1), 0},
             {0, 0, 1}};
  mat R_back = rot * rotationMatrixFromEuler(eul7);
  mat T_back = translationMatrix(R_back, {0, 0, l1});

  mat R_neck = rotationMatrixFromEuler(eul1);
  mat R_back_inv = R_back.i();
  mat R_trazeno = R_back_inv * R_neck;
  mat T_neck = translationMatrix(R_trazeno, {(l2 + l3), 0, 0});

  mat R_shoulder_right = rotationMatrixFromEuler(eul8);
  mat R_neck_inv = R_neck.i();
  R_trazeno = R_neck_inv * R_shoulder_right;
  mat T_shoulder_right = translationMatrix(R_trazeno, {0, -l9, 0});

  mat R_shoulder_left = rotationMatrixFromEuler(eul3);
  R_neck_inv = R_neck.i();
  R_trazeno = R_neck_inv * R_shoulder_left;
  mat T_shoulder_left = translationMatrix(R_trazeno, {0, l9, 0});

  mat R_elbow_left = rotationMatrixFromEuler(eul4);
  mat R_shoulder_left_inv = R_shoulder_left.i();
  R_trazeno = R_shoulder_left_inv * R_elbow_left;
  mat T_elbow_left = translationMatrix(R_trazeno, {lu * R_trazeno(0, 0), lu * R_trazeno(1, 0), lu * R_trazeno(2, 0)});

  mat R_elbow_right = rotationMatrixFromEuler(eul5);
  mat R_shoulder_right_inv = R_shoulder_right.i();
  R_trazeno = R_shoulder_right_inv * R_elbow_right;
  mat T_elbow_right = translationMatrix(R_trazeno, {lu * R_trazeno(0, 0), lu * R_trazeno(1, 0), lu * R_trazeno(2, 0)});

  mat R_wrist_left = rotationMatrixFromEuler(eul2);
  mat R_elbow_left_inv = R_elbow_left.i();
  R_trazeno = R_elbow_left_inv * R_wrist_left;
  mat T_wrist_left = translationMatrix(R_trazeno, {lf * R_trazeno(0, 0), lf * R_trazeno(1, 0), lf * R_trazeno(2, 0)});

  mat R_wrist_right = rotationMatrixFromEuler(eul6);
  mat R_elbow_right_inv = R_elbow_right.i();
  R_trazeno = R_elbow_right_inv * R_wrist_right;
  mat T_wrist_right = translationMatrix(R_trazeno, {lf * R_trazeno(0, 0), lf * R_trazeno(1, 0), lf * R_trazeno(2, 0)});

  mat T_0 = T_back;
  mat T_1 = T_back * T_neck;
  mat T_2 = T_1 * T_shoulder_left;
  mat T_3 = T_1 * T_shoulder_right;
  mat T_4 = T_2 * T_elbow_left;
  mat T_5 = T_3 * T_elbow_right;
  mat T_6 = T_4 * T_wrist_left;
  mat T_7 = T_5 * T_wrist_right;

  vec xx1 = {T_0(0, 3), T_1(0, 3), T_2(0, 3), T_4(0, 3), T_6(0, 3), T_4(0, 3), T_2(0, 3), T_1(0, 3), T_3(0, 3), T_5(0, 3), T_7(0, 3)};
  vec yy1 = {T_0(1, 3), T_1(1, 3), T_2(1, 3), T_4(1, 3), T_6(1, 3), T_4(1, 3), T_2(1, 3), T_1(1, 3), T_3(1, 3), T_5(1, 3), T_7(1, 3)};
  vec zz1 = {T_0(2, 3), T_1(2, 3), T_2(2, 3), T_4(2, 3), T_6(2, 3), T_4(2, 3), T_2(2, 3), T_1(2, 3), T_3(2, 3), T_5(2, 3), T_7(2, 3)};
  // fali to s TF-ovima
  vec xx = xx1 + xoffset;
  vec yy = yy1 - yoffset;
  vec zz = zz1 - zoffset;

  // printf ("%.2f %.2f %.2f    %.2f %.2f %.2f   %.2f\n",xx(0),yy(0),zz(0),xx(1),yy(1),zz(1),fp);

  vec pp = -(yy / xx * fp);
  vec pp1 = zz / xx * fp;

  vec rezx = {pp(1), pp(2), pp(3), pp(4), pp(5), pp(6)};
  vec rezy = {pp1(1), pp1(2), pp1(3), pp1(4), pp1(5), pp1(6)};

  pp = 320 + pp;
  pp1 = 480 - (pp1 + 240);

  vec z = {pp(2), pp1(2), pp(8), pp1(8), pp(3), pp1(3), pp(9), pp1(9), pp(4), pp1(4), pp(10), pp1(10), pp(0), pp1(0)};

  return z;
}

/// @cond DEV
/*
 * Class EKF needs to be derived, two virtual functions are provided in
 * which system model and output model are described.
 */
class MyEKF : public EKF
{
public:
  virtual colvec f(const colvec &x, const colvec &u)
  {
    colvec exit(34);
    for (int i = 0; i < 34; i++)
    {
      exit(i) = x(i);
    }
    return exit;
  }

  virtual colvec h(const colvec &x)
  {
    return measureModel(x, inputs_);
  }
};
/// @endcond

/////////////////////////////////////////////////////////////////////////////

class KalmanFilter
{
  MyEKF trackingEKF;

  float z[14];
  float xx1[11];
  float yy1[11];
  float zz1[11];
  float angles[24];
  vec skeleton2dPoints;
  vec measurements_old;
  vec eul1 = {0, 0, 0};
  vec eul2 = {0, 0, 0};
  vec eul3 = {0, 0, 0};
  vec eul4 = {0, 0, 0};
  vec eul5 = {0, 0, 0};
  vec eul6 = {0, 0, 0};
  vec eul7 = {0, 0, 0};
  vec eul8 = {0, 0, 0};
  int frame_count = 0;
  vec inputs;
  mat points, Q;
  ros::NodeHandle nh_;
  ros::Subscriber image_sub_;
  ros::Subscriber imu0_sub_;
  ros::Subscriber imu1_sub_;
  ros::Subscriber imu2_sub_;
  ros::Subscriber imu3_sub_;
  ros::Subscriber imu4_sub_;
  ros::Subscriber imu5_sub_;
  ros::Subscriber imu6_sub_;
  ros::Subscriber imu7_sub_;
  ros::Subscriber visualanalysis_sub_;
  ros::Publisher pub_2d;
  ros::Publisher pub_3d;
  vec x0;

  void publish_2d_skeleton(vec calculated_dots);
  void publish_3d_skeleton(vec calculated_dots);
  void calculate_disntace(vec kalman, vec picture);

public:
  KalmanFilter();
  ~KalmanFilter();

  void imageCallback(const sensor_msgs::ImageConstPtr &msg);
  void imu0Callback(const sensor_msgs::ImuConstPtr &msg);
  void imu1Callback(const sensor_msgs::ImuConstPtr &msg);
  void imu2Callback(const sensor_msgs::ImuConstPtr &msg);
  void imu3Callback(const sensor_msgs::ImuConstPtr &msg);
  void imu4Callback(const sensor_msgs::ImuConstPtr &msg);
  void imu5Callback(const sensor_msgs::ImuConstPtr &msg);
  void imu6Callback(const sensor_msgs::ImuConstPtr &msg);
  void imu7Callback(const sensor_msgs::ImuConstPtr &msg);
  void viusalanalysisCallback(const visualanalysis_msgs::BodyJoint2DArrayConstPtr &msg);

  void getDots();
  void getInputs();
  void drawDots(vec, cv::Mat, cv::MarkerTypes, cv::Scalar);
};

KalmanFilter::KalmanFilter()
{
  // KalmanFilter::getDots();
  x0.set_size(34);
  // x0 = {4.552619136921988, -0.248755830078776, 0.201086244627065, 6578.53392607768, 0.0547225587133672, 0.0562580301131174, 0.0534298759573203, -0.0570678370440817, -0.00706783704405687, 0.178183201797646, -1.67918989909384, -6.25629256627128, 3.15561283142777, -1.82855706562759, -0.0649516229996412, 0, -2.35026079433415, 2.79760357865585, -2.00464558423821, 2.19301640294926, 0.177372998940544, -0.294553749159429, 1.69122002843088, 0.205504150366604, 0.598289544515621, -2.22122055606182, -0.0889752139334456, 0, 0.986992331043743, -0.103143931802163, -1.76697417202578, 1.49160013229955, -0.0433720514870115, 2.11153913895151};

  x0 = {/*
0.806798205677359,
0.0886935242011312,
0.409821901169479,
500,
0.149873689686551,
0.116630173819200,
0.109194574646724,
0.131685831439713,
0.181685831457011,
0.333148609821074,
0.00391015548560084,
0.00265181992251741,
-0.00169727126581536,
-0.000299353637577672,
-0.000466321727044695,
0,
-6.08093083727795e-11,
2.53247581215569e-11,
-3.95682140802321e-11,
0.00123990248435844,
-0.00552453325112062,
-1.60615968025549e-11,
0.00411189231156139,
-0.0120433671987988,
-9.35591430349894e-14,
-0.00227324886306139,
-0.00467556210681452,
0,
0.207499584575314,
-0.153307000463743,
-0.0957548559197653,
3.97844021270531e-11,
-1.10161847111873e-10,
1.45321162213724e-10*/

        8.0680e-01,
        8.8694e-02,
        4.0982e-01,
        5.0000e+02,
        1.4987e-01,
        1.1663e-01,
        1.0919e-01,
        1.3169e-01,
        1.8169e-01,
        3.3315e-01,
        3.9102e-03,
        2.6518e-03,
        -1.6973e-03,
        -2.9935e-04,
        -4.6632e-04,
        0,
        -6.0809e-11,
        2.5325e-11,
        -3.9568e-11,
        1.2399e-03,
        -5.5245e-03,
        -1.6062e-11,
        4.1119e-03,
        -1.2043e-02,
        -9.3559e-14,
        -2.2732e-03,
        -4.6756e-03,
        0,
        2.0750e-01,
        -1.5331e-01,
        -9.5755e-02,
        3.9784e-11,
        -1.1016e-10,
        1.4532e-10

  };

  Q.eye(34, 34);
  Q = Q * 0.00000001;
  Q(0, 0) = Q(1, 1) = Q(2, 2) = Q(3, 3) = 1;
  mat R;
  R = R.eye(14, 14) * 1000;
  trackingEKF.InitSystem(34, 14, Q, R);
  trackingEKF.InitSystemState(x0);
  // trackingEKF.InitSystemStateCovariance(P0); nezz sto je ovo
  cout << "INICIJALIZIRAN SAM" << endl;

  image_sub_ = nh_.subscribe("/camera/image_raw", 1, &KalmanFilter::imageCallback, this);
  imu0_sub_ = nh_.subscribe("/imu0", 1, &KalmanFilter::imu0Callback, this);
  imu1_sub_ = nh_.subscribe("/imu1", 1, &KalmanFilter::imu1Callback, this);
  imu2_sub_ = nh_.subscribe("/imu2", 1, &KalmanFilter::imu2Callback, this);
  imu3_sub_ = nh_.subscribe("/imu3", 1, &KalmanFilter::imu3Callback, this);
  imu4_sub_ = nh_.subscribe("/imu4", 1, &KalmanFilter::imu4Callback, this);
  imu5_sub_ = nh_.subscribe("/imu5", 1, &KalmanFilter::imu5Callback, this);
  imu6_sub_ = nh_.subscribe("/imu6", 1, &KalmanFilter::imu6Callback, this);
  imu7_sub_ = nh_.subscribe("/imu7", 1, &KalmanFilter::imu7Callback, this);
  visualanalysis_sub_ = nh_.subscribe("/drone/visualanalysis/human_pose_2d", 1, &KalmanFilter::viusalanalysisCallback, this);
  pub_2d = nh_.advertise<visualanalysis_msgs::BodyJoint2DArray>("kalman_2d_skeleton_array", 1000);
  pub_3d = nh_.advertise<visualanalysis_msgs::BodyJoint3DArray>("kalman_3d_skeleton_array", 1000);
}

KalmanFilter::~KalmanFilter()
{
}
void KalmanFilter::viusalanalysisCallback(const visualanalysis_msgs::BodyJoint2DArrayConstPtr &msg)
{
  std::vector<visualanalysis_msgs::BodyJoint2D> skeleton = msg->skeleton_2d;
  int i = 0;
  skeleton2dPoints.set_size(17 * 3);
  for (std::vector<visualanalysis_msgs::BodyJoint2D>::iterator it = skeleton.begin(); it != skeleton.end(); ++it)
  {
    skeleton2dPoints(i++) = (*it).x;
    skeleton2dPoints(i++) = (*it).y;
    skeleton2dPoints(i++) = (*it).score;
  };
  // skeleton2dPoints.print("points");
  return;
}

void KalmanFilter::imageCallback(const sensor_msgs::ImageConstPtr &msg)
{
  if (frame_count++ < 10)
    return;

  KalmanFilter::getInputs();
  trackingEKF.SetInputs(inputs);
  cv::Mat image = cv_bridge::toCvShare(msg, "bgr8")->image;

  vec measurements(14);
  mat zcov;
  zcov = zcov.eye(14, 14);

  for (int i = 15, j = 0; i < 33; i = i + 3, j = j + 2)
  {
    measurements(j) = skeleton2dPoints(i);
    measurements(j + 1) = skeleton2dPoints(i + 1);
    zcov(j, j) = (1 - 0.9 * skeleton2dPoints(i + 2));
    zcov(j + 1, j + 1) = (1 - 0.9 * skeleton2dPoints(i + 2));
  }

  measurements(12) = (skeleton2dPoints(33) + skeleton2dPoints(36)) / 2;
  measurements(13) = (skeleton2dPoints(34) + skeleton2dPoints(37)) / 2;
  zcov(12, 12) = (1 - 1 * ((skeleton2dPoints(35) + skeleton2dPoints(38)) / 2));
  zcov(13, 13) = (1 - 1 * ((skeleton2dPoints(35) + skeleton2dPoints(38)) / 2));

  /*   for (int i = 15, j = 0; i < 33; i = i + 3, j = j + 2)
    {
      measurements(j) = points(frame_count, i);
      measurements(j + 1) = points(frame_count, i + 1);
      zcov(j, j) = 1 - points(frame_count, i + 2);
      zcov(j + 1, j + 1) = 1 - points(frame_count, i + 2);
    }

    measurements(12) = (points(frame_count, 33) + points(frame_count, 36)) / 2;
    measurements(13) = (points(frame_count, 34) + points(frame_count, 37)) / 2;
    zcov(12, 12) = 1 - ((points(frame_count, 35) + points(frame_count, 38)) / 2);
    zcov(13, 13) = 1 - ((points(frame_count, 35) + points(frame_count, 38)) / 2); */
  // zcov.print("zcov");

  measurements_old = measurements;

  trackingEKF.EKalmanf(measurements, inputs);

  vec *x_pred = trackingEKF.GetCurrentState();
  mat *P_pred = trackingEKF.GetCurrentR();
  vec *x_pred2 = trackingEKF.GetCurrentEstimatedState();
  //  x_pred2->save("x.csv", csv_ascii);
  trackingEKF.SetQ(zcov);

  vec *x_corr = trackingEKF.GetCurrentEstimatedOutput();

  publish_2d_skeleton(*x_corr);
  // x_pred->print("x_pred");
  // x_corr->print("x_corr");
  //  x_pred2->print("x_pred2");
  //  x0.print("x0");
  // x_corr->print("Pred");
  //   trackingEKF.InitSystem(34, 14, zcov, *P_pred);
  //   trackingEKF.InitSystemState(*x_pred);

  vec measuremodel = measureModel(x0, inputs);
  // incijalzation

  // EKF correct

  // measureModel(*x_corr, inputs);

  try
  {
    KalmanFilter::drawDots(measurements, image, cv::MARKER_DIAMOND, Scalar(0, 0, 256));
    KalmanFilter::drawDots(*x_corr /* measuremodel*/, image, cv::MARKER_TILTED_CROSS, Scalar(0, 256, 0));
    calculate_disntace(measurements, *x_corr);
    // cout << image.size() << endl;
    cv::imshow("view", image);
    cv::waitKey(1);
    // cout << "frame count: " << frame_count << endl;
  }
  catch (cv_bridge::Exception &e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
}

void KalmanFilter::imu0Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul1 = {roll, pitch, yaw};
}

void KalmanFilter::imu1Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul2 = {roll, pitch, yaw};
}

void KalmanFilter::imu2Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul3 = {roll, pitch, yaw};
}

void KalmanFilter::imu3Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul4 = {roll, pitch, yaw};
}

void KalmanFilter::imu4Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul5 = {roll, pitch, yaw};
}

void KalmanFilter::imu5Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul6 = {roll, pitch, yaw};
}

void KalmanFilter::imu6Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul7 = {roll, pitch, yaw};
}

void KalmanFilter::imu7Callback(const sensor_msgs::ImuConstPtr &msg)
{
  tf::Quaternion q(
      msg->orientation.x,
      msg->orientation.y,
      msg->orientation.z,
      msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getEulerYPR(yaw, pitch, roll);
  eul8 = {roll, pitch, yaw};
}

void KalmanFilter::getInputs()
{

  inputs.set_size(24);

  inputs(0) = eul1(0);
  inputs(1) = eul1(1);
  inputs(2) = eul1(2);
  inputs(3) = eul2(0);
  inputs(4) = eul2(1);
  inputs(5) = eul2(2);
  inputs(6) = eul3(0);
  inputs(7) = eul3(1);
  inputs(8) = eul3(2);
  inputs(9) = eul4(0);
  inputs(10) = eul4(1);
  inputs(11) = eul4(2);
  inputs(12) = eul5(0);
  inputs(13) = eul5(1);
  inputs(14) = eul5(2);
  inputs(15) = eul6(0);
  inputs(16) = eul6(1);
  inputs(17) = eul6(2);
  inputs(18) = eul7(0);
  inputs(19) = eul7(1);
  inputs(20) = eul7(2);
  inputs(21) = eul8(0);
  inputs(22) = eul8(1);
  inputs(23) = eul8(2);
  // cout << "inputs gatherd." << endl;
}

void KalmanFilter::getDots()
{
  cout << "uso" << endl;
  points.set_size(3961, 34 + 17);
  ifstream inFile;
  inFile.open("/home/goran/dots.txt");
  if (!inFile)
  {
    cerr << "Unable to open file dots.txt";
    exit(1); // call system to stop
  }

  int num_lines = 0;
  string number;
  for (int i = 0; i < 3961; i++)
  {
    string x;
    string y;
    string v;
    inFile >> x;
    inFile >> y;
    inFile >> v;
    points(i, 0) = std::stof(x.erase(0, 2));
    points(i, 1) = std::stof(y);
    points(i, 2) = std::stof(v.erase(14, 14));
    int count = 3;
    for (int j = 0; j < 16; j++)
    {
      inFile >> x;
      inFile >> y;
      inFile >> v;
      points(i, count++) = std::stof(x.erase(0, 1));
      points(i, count++) = std::stof(y);
      points(i, count++) = std::stof(v.erase(14, 14));
    }
    points(i, count - 1) = std::stof(v.erase(14, 14));
  }

  cout << "points gatherd." << endl;
}

void KalmanFilter::drawDots(vec measurments, cv::Mat image, cv::MarkerTypes markerType, Scalar color)
{
  for (int i = 0; i < measurments.size(); i = i + 2)
  {
    cv::drawMarker(image, Point(measurments(i), measurments(i + 1)), color, markerType);
  }

  cv::line(image, Point(measurments(8), measurments(9)), Point(measurments(4), measurments(5)), color, 2);
  cv::line(image, Point(measurments(0), measurments(1)), Point(measurments(4), measurments(5)), color, 2);
  cv::line(image, Point(measurments(0), measurments(1)), Point(measurments(2), measurments(3)), color, 2);
  cv::line(image, Point(measurments(2), measurments(3)), Point(measurments(6), measurments(7)), color, 2);
  cv::line(image, Point(measurments(6), measurments(7)), Point(measurments(10), measurments(11)), color, 2);
}

void KalmanFilter::publish_2d_skeleton(vec dots)
{
  std::vector<visualanalysis_msgs::BodyJoint2D> skeletonArray;
  visualanalysis_msgs::BodyJoint2D bodyJoint;
  for (int i = 0; i < 14; i = i + 2)
  {
    bodyJoint.x = dots(i);
    bodyJoint.y = dots(i + 1);
    skeletonArray.push_back(bodyJoint);
  }
  visualanalysis_msgs::BodyJoint2DArray skeleton2Darray;
  skeleton2Darray.skeleton_2d = skeletonArray;
  pub_2d.publish(skeleton2Darray);
  ros::spinOnce();
}

void KalmanFilter::publish_3d_skeleton(vec dots)
{
  std::vector<visualanalysis_msgs::BodyJoint3D> skeletonArray;
  visualanalysis_msgs::BodyJoint3D bodyJoint;
  for (int i = 0; i < 24; i = i + 3)
  {
    bodyJoint.x = dots(i);
    bodyJoint.y = dots(i + 1);
    bodyJoint.z = dots(i + 1);
    skeletonArray.push_back(bodyJoint);
  }
  visualanalysis_msgs::BodyJoint3DArray skeleton3Darray;
  skeleton3Darray.skeleton_3d = skeletonArray;
  pub_3d.publish(skeleton3Darray);
  ros::spinOnce();
}

void KalmanFilter::calculate_disntace(vec kalman, vec picture)
{
  vec distance(7);
  for (int i = 0; i < 7; ++i)
  {
    distance(i) = sqrt(pow(kalman(i * 2) - picture(i * 2), 2) + pow(kalman(i * 2 + 1) - picture(i * 2 + 1), 2));
  }
  string filename("tmp1.txt");
  ofstream file_out;

  file_out.open(filename, std::ios_base::app);
  file_out << distance(0) << " " << distance(1) << " " << distance(2) << " " << distance(3) << " " << distance(4) << " " << distance(5) << " " << distance(6) << endl;
  return;
}
int main(int argc, char **argv)
{
  ros::init(argc, argv, "kalman_node");
  KalmanFilter kalmanF;
  ros::spin();
  return 0;
}
