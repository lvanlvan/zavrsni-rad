import matplotlib.pyplot as plt
  
x = []
y = []
i = 0
for line in open('tmp1.txt', 'r'):
    lines = [i for i in line.split()]
    x.append(i)
    i  = i+1
    y.append(float(lines[0]))
      
plt.title("left shoulder")
plt.xlabel('frame id')
plt.ylabel('distance')
plt.plot(x, y)
  
plt.show()