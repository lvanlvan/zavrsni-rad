angle=0;
rot=[1 0 0 0;
    0 cos(angle) -sin(angle) 0;
    0 sin(angle) cos(angle) 0;
    0 0 0 1];
wanted_time=24;
t1=find(abs(wanted_time-time1)==min(abs(wanted_time-time1)));
t1=t1(1)
t2=find(abs(wanted_time-time2)==min(abs(wanted_time-time2)));
t2=t2(1);
t3=find(abs(wanted_time-time3)==min(abs(wanted_time-time3)));
t3=t3(1);
t4=find(abs(wanted_time-time4)==min(abs(wanted_time-time4)));
t4=t4(1);
t5=find(abs(wanted_time-time5)==min(abs(wanted_time-time5)));
t5=t5(1);
t6=find(abs(wanted_time-time6)==min(abs(wanted_time-time6)));
t6=t6(1);
t7=find(abs(wanted_time-time7)==min(abs(wanted_time-time7)));
t7=t7(1);
t8=find(abs(wanted_time-time8)==min(abs(wanted_time-time8)));
t8=t8(1);

lu = 0.30; %upper_arm
lf = 0.23; %forward_arm
l9 = 0.15; %clavicle
l3 = 0.2; 
l2 = 0.25;
l1 = 1.0;
R_back=eul2rotm(eul7(t7,1:3));
T_back(1:4,4)=[0 0 l1 1]';
T_back(1:3,1:3)=R_back;

R_neck=eul2rotm(eul1(t1,1:3));
R_back_inv = inv(R_back(:,:));
R_trazeno=R_back_inv*R_neck;
T_neck(1:4,4)=[l2+l3 0 0 1]';
T_neck(1:3,1:3)=R_trazeno;


R_shoulder_right=eul2rotm(eul8(t8,1:3));
R_neck_inv = inv(R_neck(:,:));
R_trazeno=R_neck_inv*R_shoulder_right;
T_shoulder_right(1:4,4)=[0 0 l9 1]';
T_shoulder_right(1:3,1:3)=R_trazeno;



R_shoulder_left=eul2rotm(eul3(t3,1:3));
R_neck_inv = inv(R_neck(:,:));
R_trazeno=R_neck_inv*R_shoulder_left;
T_shoulder_left(1:4,4)=[0 0 -l9 1]';
T_shoulder_left(1:3,1:3)=R_trazeno;


R_elbow_left=eul2rotm(eul5(t5,1:3));
R_shoulder_left_inv = inv(R_shoulder_left(:,:));
R_trazeno=R_shoulder_left_inv*R_elbow_left;
T_elbow_left(1:4,4)=[lu*R_trazeno(1,1) lu*R_trazeno(2,1) lu*R_trazeno(3,1) 1];
T_elbow_left(1:3,1:3)=R_trazeno;


R_elbow_right=eul2rotm(eul4(t4,1:3));
R_shoulder_right_inv = inv(R_shoulder_right(:,:));
R_trazeno=R_shoulder_right_inv*R_elbow_right;
T_elbow_right(1:4,4)=[lu*R_trazeno(1,1) lu*R_trazeno(2,1) lu*R_trazeno(3,1) 1];
T_elbow_right(1:3,1:3)=R_trazeno;



R_wrist_left=eul2rotm(eul5(t5,1:3));
R_elbow_left_inv = inv(R_elbow_left(:,:));
R_trazeno=R_elbow_left_inv*R_wrist_left;
T_wrist_left(1:4,4)=[lf*R_trazeno(1,1) lf*R_trazeno(2,1) lf*R_trazeno(3,1) 1];
T_wrist_left(1:3,1:3)=R_trazeno;

R_wrist_right=eul2rotm(eul2(t2,1:3));
R_elbow_right_inv = inv(R_elbow_right(:,:));
R_trazeno=R_elbow_right_inv*R_wrist_right;
T_wrist_right(1:4,4)=[lf*R_trazeno(1,1) lf*R_trazeno(2,1) lf*R_trazeno(3,1) 1];
T_wrist_right(1:3,1:3)=R_trazeno;

T_0=T_back*rot;
T_1=T_back*T_neck*rot;
T_2=T_1*T_shoulder_left*rot;
T_3=T_1*T_shoulder_right*rot;
T_4=T_2*T_elbow_left*rot;
T_5=T_3*T_elbow_right*rot;
T_6=T_4*T_wrist_left*rot;
T_7=T_5*T_wrist_right*rot;

xx1=[T_0(1,4) T_1(1,4) T_2(1,4) T_4(1,4) T_6(1,4) T_4(1,4) T_2(1,4) T_1(1,4) T_3(1,4) T_5(1,4) T_7(1,4)];
yy1=[T_0(1,4) T_1(2,4) T_2(2,4) T_4(2,4) T_6(2,4) T_4(2,4) T_2(2,4) T_1(2,4) T_3(2,4) T_5(2,4) T_7(2,4)];
zz1=[T_0(1,4) T_1(3,4) T_2(3,4) T_4(3,4) T_6(3,4) T_4(3,4) T_2(3,4) T_1(3,4) T_3(3,4) T_5(3,4) T_7(3,4)];

%xx=[T_0(1,4) T_1(1,4) T_3(1,4) T_5(1,4) T_7(1,4)];
%yy=[T_0(1,4) T_1(2,4) T_3(2,4) T_5(2,4) T_7(2,4)];
%zz=[T_0(1,4) T_1(3,4) T_3(3,4) T_5(3,4) T_7(3,4)];

