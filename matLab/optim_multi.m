
rangey=[11 12 13 14 15 16 17 18 19 20 21 22];  %selected points on the body
global count ydata;

frames=[100 190 600 570  680 350 1]; %frames for inital optimization
frames=1:20:1100
frames=[1100 1 680];
len=length(frames);
ydata=[];  % 7 points from image for different frames
xdata=[];  % 24 angles from multiple frames
for i=1:length(frames)
    j=frames(i);
    wanted_time=timecam(j); 
    get_inputs
    ydata=[ydata;points1(j,rangey)'];
    y0=(points1(j,23:24)+points1(j,25:26))/2;
    ydata=[ydata; y0']; %image points p5(x,y) p6(x,y) p7(x,y) p8(x,y) p9(x,y) p10(x,y) (p11(x,y)+p12(x,y))/2
    xdata=[xdata inputs];
end
ydata=[ydata; zeros(24,1)];
clear x0
        
x0=[500 0.3 0.23 0.15 0.2 0.25 1.0]; % f lu lf l9 l3 l2 l1
x0=[x0 zeros(1,24)]; %each angle offset
for i=1:length(frames)
    x0=[x0 1.5 0 1.2];  %xoffset yoffset zoffset
end
xcurrent=x0;
count=0;
[x y]=lsqcurvefit(@krit_multi,x0,xdata,ydata);

