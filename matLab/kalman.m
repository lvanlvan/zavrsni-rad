%states
%x y z f lu l9 l3 l2 l1 eul1_offset eul2_offset eul3_offset ... eul24_offset
%inputs
%eul1 ... eul24
%outputs
%p5(x,y) p6(x,y) p7(x,y) p8(x,y) p9(x,y) p10(x,y) (p11(x,y)+p12(x,y))/2


timecam2 = cellfun(@(m) double(m.Header.Stamp.Sec),msgStructs2_new) +cellfun(@(m) double(m.Header.Stamp.Nsec),msgStructs2_new)/1000000000;
timecam2 = timecam2 - time10;

x0=[x(32:34) x(1:31)];
%x0=[x(32:34) x(1:7) zeros(1,24)];
Q = eye(34)*0.00000001;
Q(1,1) = 1;
Q(2,2) = 1;
Q(3,3) = 1;
Q(4,4) = 1;
trackingEKF()
EKF = trackingEKF(@stateModel, @measureModel, x0,'MeasurementNoise',eye(14)*1000,'ProcessNoise',Q);
EKF.StateCovariance
zcov=eye(14)*1000;
xcurrent = [x(32:34) x(1:7) x(8:31)];%zeros(1,24)];
movement = [];
for i=1:1500
    wanted_time=timecam(i);
    
    t_cam=find(abs(wanted_time-timecam2)==min(abs(wanted_time-timecam2)));
    t_cam=t_cam(1);    
    
    get_inputs
    rangey = [11 12 13 14 15 16 17 18 19 20 21 22]; 
    measurements = points1(i,rangey)';
    y0=(points1(1,23:24)+points1(1,25:26))/2;
    measurements=[measurements; y0']; 

    [x_pred, P_pred] = predict(EKF, inputs);
    zcov=eye(14)*1000;
    bad=find(measurements==-1);


    if (length(bad)>0)
        for k=1:length(bad)
            zcov(bad(k),bad(k))=100000;
        end
        measurements(bad)=measurements_old(bad);
    else
        measurements_old=measurements;
    end
    initialize(EKF,x_pred,P_pred,'MeasurementNoise',zcov);
    
    [x_corr, P_corr] = correct(EKF, measurements,inputs);
    movement(i,:) = x_corr;

    measuremodel = measureModel(x_corr,inputs);
    yy = measuremodel(1:2:length(measuremodel));
    zz = measuremodel(2:2:length(measuremodel));
    i
    im1=decodeJpeg(msgStructs3{i}.Data);
%    im1=decodeJpeg(msgStructs2_new{i}.Data);
    %subplot(2,2,1);
    imshow(im1)
    hold on;
    k = ishold;
    plot(yy,zz,'bx-','MarkerSize',10);    
    plot(measurements(1:2:12),measurements(2:2:12),'o','MarkerSize',10)
    hold off
%     [xx1 yy1 zz1]=get_xyz(inputs,x0);
%     subplot(2,2,2);
%     plot(yy1,zz1);
%     xlabel('x');ylabel('y');
%     subplot(2,2,3);
%     plot(xx1,zz1);
%     xlabel('x');ylabel('z');
%     subplot(2,2,4);
%     plot3(xx1,yy1,zz1);
%     xlabel('x');ylabel('y');zlabel('z');    
%             daspect([1 1 1])
    pause(0.01);
   
end