% lu = 0.30; %upper_arm
% lf = 0.23; %forward_arm
% l9 = 0.15; %clavicle
% l3 = 0.5;%0.2; 
% l2 = 0.25;
% l1 = 1.0;
angles;
eul1 = angles(1:3);
eul2 = angles(4:6);
eul3 = angles(7:9);
eul4 = angles(10:12);
eul5 = angles(13:15);
eul6 = angles(16:18);
eul7 = angles(19:21);
eul8 = angles(22:24);
%eul7=eulerTransformation(eul7,[0 0 0.2]);
rot_angle=1;%1.57;
rot=[cos(rot_angle) -sin(rot_angle) 0;
    sin(rot_angle) cos(rot_angle) 0
    0 0  1];

R_back = eul2rotm(eul7);
T_back(1:4,4) = [0 0 l1 1]';
T_back(1:3,1:3) = rot*R_back;

R_neck=eul2rotm(eul1);
R_back_inv = inv(R_back(:,:));
R_trazeno=R_back_inv*R_neck;
T_neck(1:4,4) = [(l2+l3) 0 0 1]';
T_neck(1:3,1:3) = R_trazeno;


R_shoulder_right = eul2rotm(eul8);
R_neck_inv = inv(R_neck(:,:));
R_trazeno = R_neck_inv * R_shoulder_right;
T_shoulder_right(1:4,4) = [l9*R_trazeno(1,1) l9*R_trazeno(2,1) l9*R_trazeno(3,1) 1]';
T_shoulder_right(1:4,4)=[0 -l9 0 1]';

T_shoulder_right(1:3,1:3)=R_trazeno;



R_shoulder_left = eul2rotm(eul3);
R_neck_inv = inv(R_neck(:,:));
R_trazeno = R_neck_inv * R_shoulder_left;
T_shoulder_left(1:4,4) = [0 l9 0 1]';
T_shoulder_left(1:3,1:3) = R_trazeno;


R_elbow_left = eul2rotm(eul4);
R_shoulder_left_inv = inv(R_shoulder_left(:,:));
R_trazeno = R_shoulder_left_inv * R_elbow_left;
T_elbow_left(1:4,4) = [lu*R_trazeno(1,1) lu*R_trazeno(2,1) lu*R_trazeno(3,1) 1];

T_elbow_left(1:3,1:3) = R_trazeno;


R_elbow_right = eul2rotm(eul5);
R_shoulder_right_inv = inv(R_shoulder_right(:,:));
R_trazeno = R_shoulder_right_inv*R_elbow_right;
T_elbow_right(1:4,4) = [lu*R_trazeno(1,1) lu*R_trazeno(2,1) lu*R_trazeno(3,1) 1];

T_elbow_right(1:3,1:3) = R_trazeno;



R_wrist_left = eul2rotm(eul2);
R_elbow_left_inv = inv(R_elbow_left(:,:));
R_trazeno = R_elbow_left_inv*R_wrist_left;
T_wrist_left(1:4,4) = [lf*R_trazeno(1,1) lf*R_trazeno(2,1) lf*R_trazeno(3,1) 1];
T_wrist_left(1:3,1:3) = R_trazeno;

R_wrist_right = eul2rotm(eul6);
R_elbow_right_inv = inv(R_elbow_right(:,:));
R_trazeno = R_elbow_right_inv*R_wrist_right;
T_wrist_right(1:4,4) = [lf*R_trazeno(1,1) lf*R_trazeno(2,1) lf*R_trazeno(3,1) 1];
T_wrist_right(1:3,1:3) = R_trazeno;

T_0 = T_back;
T_1 = T_back * T_neck;
T_2 = T_1 * T_shoulder_left;
T_3 = T_1 * T_shoulder_right;
T_4 = T_2 * T_elbow_left;
T_5 = T_3 * T_elbow_right;
T_6 = T_4 * T_wrist_left;
T_7 = T_5 * T_wrist_right;


xx1 = [T_0(1,4) T_1(1,4) T_2(1,4) T_4(1,4) T_6(1,4) T_4(1,4) T_2(1,4) T_1(1,4) T_3(1,4) T_5(1,4) T_7(1,4)];
yy1 = [T_0(2,4) T_1(2,4) T_2(2,4) T_4(2,4) T_6(2,4) T_4(2,4) T_2(2,4) T_1(2,4) T_3(2,4) T_5(2,4) T_7(2,4)];
zz1 = [T_0(3,4) T_1(3,4) T_2(3,4) T_4(3,4) T_6(3,4) T_4(3,4) T_2(3,4) T_1(3,4) T_3(3,4) T_5(3,4) T_7(3,4)];


xx = xx1 + xoffset;
yy = yy1 - yoffset;
zz = zz1 - zoffset;
pp = -yy./xx*fp;
pp1 = zz./xx*fp;
rezx = [pp(2) pp(3) pp(4) pp(5) pp(6) pp(7)];
rezy=[pp1(2) pp1(3) pp1(4) pp1(5) pp1(6) pp1(7)];
% subplot(2,2,1);
% plot3(xx,yy,zz,'x-');xlim([-3 3]);ylim([-3 3]);zlim([-3 3]);xlabel('x');ylabel('y');zlabel('z')
% subplot(2,2,2);
% plot(xx,yy);xlim([-3 3]);ylim([-3 3]);xlabel('x');ylabel('y');
% subplot(2,2,3);
% plot(xx,zz);xlim([-3 3]);ylim([-3 3]);xlabel('x');ylabel('x');
% subplot(2,2,4);
% plot(yy,zz);xlim([-3 3]);ylim([-3 3]);xlabel('y');ylabel('z');

%plot(yy,zz,'x-');

